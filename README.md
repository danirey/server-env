# Flectra Community / server-env

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[server_environment_data_encryption](server_environment_data_encryption/) | 2.0.1.0.2| Server Environment Data Encryption
[server_environment](server_environment/) | 2.0.2.1.1| move some configurations out of the database
[data_encryption](data_encryption/) | 2.0.1.0.0| Store accounts and credentials encrypted by environment
[payment_environment](payment_environment/) | 2.0.1.0.2| Configure payment acquirers with server_environment
[mail_environment](mail_environment/) | 2.0.1.0.0| Configure mail servers with server_environment_files
[auth_saml_environment](auth_saml_environment/) | 2.0.1.0.0| Allows system administrator to authenticate with any account
[server_environment_iap](server_environment_iap/) | 2.0.1.0.0| Configure IAP Account with server_environment_files
[server_environment_ir_config_parameter](server_environment_ir_config_parameter/) | 2.0.1.1.0|         Override System Parameters from server environment file


