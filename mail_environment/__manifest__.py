# Copyright 2012-2018 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl)

{
    "name": "Mail configuration with server_environment",
    "version": "2.0.1.0.0",
    "category": "Tools",
    "summary": "Configure mail servers with server_environment_files",
    "author": "Camptocamp, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/server-env",
    "depends": ["fetchmail", "server_environment"],
}
